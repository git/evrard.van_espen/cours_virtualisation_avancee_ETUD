#+TITLE: TP1 - Cours de virtualisation avancée - /Proxmox/
#+OPTIONS: toc:nil date:nil author:nil
#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [12pt,a4paper]
#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER: \usepackage[inkscapelatex=false]{svg}
#+LATEX_HEADER: \usepackage[sfdefault]{AlegreyaSans}
#+LATEX_HEADER: \usepackage{multicol}
#+LATEX_HEADER: \usepackage{minted}
#+LATEX_HEADER: \usepackage{float}
#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usetikzlibrary{positioning}
#+LATEX_HEADER: \renewcommand\listingscaption{Exemple de code}

#+BEGIN_EXPORT latex
{\large{Lire l'intégralité du TP avant de commencer quoi que ce soit !}}
\newpage
#+END_EXPORT

L'objectif de ce TP est de découvrir l'environnement de virtualisation "/Proxmox/" en mettant en place vous même plusieurs machines type /*conteneurs*/ qui vont devoir communiquer ensemble.

Lors de ce TP, vous allez mettre en place des *conteneurs* (abrégés *CT*) plutôt que des machines virtuelles afin de gagner du temps.
Cependant, gardez bien en tête que *conteneurs* et *machines virtuelles* sont certes similaires mais très différents !
Dans l'environnement /Proxmox/, les gestions des conteneurs et des machines virtuelles sont très similaires et par conséquent, savoir manipuler l'un permet aussi de savoir comment manipuler l'autre.

* Travail à réaliser

#+drawio: "schema.drawio"
#+ATTR_LATEX: :width 6cm
[[file:./images/schema-0.svg]]


Votre objectif lors de ce TP va être de mettre en place l'architecture ci dessus.
Il va vous falloir mettre en place deux *conteneurs*, dont un sera connecté à internet et un second, qui communiquera avec le premier via un réseau privé.

Sur le premier conteneur, il vous faudra installer et configurer le serveur /Nginx/ qui fera office de /proxy/ /HTTP/ pour permettre l'accès au site /web/ hébergé sur le second conteneur.

\medskip

Au début de la séance de TP, j'attribuerai à chacun·es de vous trois adresses /IP/ que vous devrez configurer sur les machines, qui seront ensuite nommées "IP publique proxy", "IP privée proxy" et "IP application".
N'oubliez pas de les noter ;)

\medskip

L'adresse de l'interface /web/ de l'instance /Proxmox/ que vous utiliserez est disponible ici: [[https://pve.virtuiut.online/][\url{https://pve.virtuiut.online/}]].

Dans l'écran de connexion, veillez à ce que le champ /"Realm"/ soit sur /"Proxmox VE authentication server"/.
Aussi, une alerte quant à la licence de /Proxmox/ peut s'afficher.
Si tel est le cas, cliquez sur /"Ok"/ et poursuivez.

\medskip

Pour vous y connecter, utilisez comme mot de passe et comme utilisateur votre nom et prénom, le tout attaché et sans accents, tirets ou espaces.

\medskip

Par exemple, je m’appelle /"VAN ESPEN Évrard"/, j'utiliserai donc =vanespenevrard= comme *identifiant et comme mot de passe*.

\medskip

Si tout a été réalisé correctement, la page /web/ présente dans le serveur "application" devrait être accessible à l'adresse =<votre identifiant>.pve.virtuiut.online=.

* Votre mission en étapes
- Vous devrez créer les deux machines, avec pour chacune un cœur /CPU/ et 512MB de mémoire vive;
- Vous devrez ensuite configurer les machines pour permettre la communication entre elles;
- Vous devrez enfin installer /sur chaque conteneur/ le paquet /nginx/ (veillez bien à le (re)démarrer à chaque modification de sa configuration (=systemd=)), puisqu'il sera utilisé une fois comme serveur /web/, et une fois comme /reverse proxy/. Sur quel conteneur tourne chacun des /nginx/ ? ;
- Éditez le fichier =/var/www/html/index.nginx-debian.html= en mettant votre nom dans la page.

* Champs à configurer
Lors de la création du conteneur, voici les champs que vous aurez à configurer.
Les autres champs que vous rencontrerez sont à laisser à leur valeur défaut.

** Onglet /"General"/
- /"Hostname"/
- /"Password"/ et /"Confirm password"/ -> ce sera le mot de passe de l'utilisateur =root= dans le conteneur (5 caractères minimum)
- /"Resource pool"/ -> sélectionnez la /pool/ à votre nom
  
** Onglet /"Template"/
- /"Template"/

** Onglet /"Disk"/
- /"Disk size"/ -> mettre =1GiB=
  
** Onglet /"CPU"/
- /"Cores"/
  
** Onglet /"Memory"/
- /"Memory"/
  
** Onglet /"Network"/
- /"Bridge"/
- /"IPv4/CIDR"/
- /"Gateway"/ -> ici mettre =192.168.1.1= ou =192.168.10.1= selon l'ip
  

* Notes et astuces
- N'oubliez pas, la machine /"proxy"/ doit avoir deux interfaces réseau, une pour le réseau public et une pour le réseau privé;
- Dans /Proxmox/, les conteneurs sont souvent nommés =CT=;
- Lors de la création du conteneur, vous n'avez pas à vous préoccuper de chaque champ de l'interface. Voir plus bas la liste des champs que vous devrez configurer;
- La machine /"proxy"/ doit utiliser l'interface =vmbr0= et avoir comme adresse /IP/ l'adresse "IP publique proxy" ainsi que l'interface =vmbr1= et avoir comme adresse /IP/ l'adresse "IP privée proxy";
- La machine /"application"/ devra utiliser l'interface =vmbr1= et avoir comme adresse /IP/ l'adresse "IP application";
- Le fichier de configuration de /nginx/ se situe dans le répertoire  =/etc/nginx/site-enabled/= et se nomme =default=;
- Voici un fichier de configuration pour /nginx/ qui permet de l'utiliser en serveur /proxy/ comme demandé (replacez le contenu actuel du fichier):

  \newpage
  
  #+BEGIN_EXAMPLE
  server {
      listen 80;
      server_name <votre identifiant proxmox>.pve.virtuiut.online;

      location / {
          proxy_pass http://<adresse IP application>:80;
      }
  }
  #+END_EXAMPLE
- Vous n'avez pas besoin de modifier la configuration de /nginx/ sur la machine "applicatif", par contre veilliez bien à le démarrer (=systemd=). Il sera accessible sur le port =80=.
- Les serveurs /nginx/ vont-ils démarrer au /boot/ du conteneur ?


\newpage

* Adresses /IP/ à utiliser
| Groupe      | Nom                  | IP publique proxy | IP privée proxy  | IP application   |
|-------------+----------------------+-------------------+------------------+------------------|
| PM 1  | ANDRÉ Eloan          | =192.168.1.2=     | =192.168.10.2=   | =192.168.10.3=   |
| PM 1  | BALLANDRAS Pierre    | =192.168.1.3=     | =192.168.10.4=   | =192.168.10.5=   |
| PM 1  | BOILEAU Nathan       | =192.168.1.4=     | =192.168.10.6=   | =192.168.10.7=   |
| PM 1  | BOUDOUL Anna         | =192.168.1.5=     | =192.168.10.8=   | =192.168.10.9=   |
| PM 1  | DE LA FUENTE Axel    | =192.168.1.6=     | =192.168.10.10=  | =192.168.10.11=  |
| PM 1  | KARTAL Emre          | =192.168.1.7=     | =192.168.10.12=  | =192.168.10.13=  |
| PM 1  | KHEDAIR Rami         | =192.168.1.8=     | =192.168.10.14=  | =192.168.10.15=  |
| PM 1  | LIVET Hugo           | =192.168.1.9=     | =192.168.10.16=  | =192.168.10.17=  |
| PM 1  | PERRET Loris         | =192.168.1.10=    | =192.168.10.18=  | =192.168.10.19=  |
| PM 1  | PINTRAND Aurelien    | =192.168.1.11=    | =192.168.10.20=  | =192.168.10.21=  |
| PM 1  | SQUIZZATO Paul       | =192.168.1.12=    | =192.168.10.22=  | =192.168.10.23=  |
| PM 1  | VALIN Arthur         | =192.168.1.13=    | =192.168.10.24=  | =192.168.10.25=  |

| Groupe      | Nom                  | IP publique proxy | IP privée proxy  | IP application   |
|-------------+----------------------+-------------------+------------------+------------------|
| PM 2  | BONNEAU Baptiste     | =192.168.1.14=    | =192.168.10.26=  | =192.168.10.27=  |
| PM 2  | BRODA Lou            | =192.168.1.15=    | =192.168.10.28=  | =192.168.10.29=  |
| PM 2  | CENTENO Matéo        | =192.168.1.16=    | =192.168.10.30=  | =192.168.10.31=  |
| PM 2  | DA COSTA CUNHA Bruno | =192.168.1.17=    | =192.168.10.32=  | =192.168.10.33=  |
| PM 2  | DELANIER Lucas       | =192.168.1.18=    | =192.168.10.34=  | =192.168.10.35=  |
| PM 2  | EVARD Lucas          | =192.168.1.19=    | =192.168.10.36=  | =192.168.10.37=  |
| PM 2  | FERREIRA Pierre      | =192.168.1.20=    | =192.168.10.38=  | =192.168.10.39=  |
| PM 2  | FRANCO Nicolas       | =192.168.1.21=    | =192.168.10.40=  | =192.168.10.41=  |
| PM 2  | GLENAT Alexandre     | =192.168.1.22=    | =192.168.10.42=  | =192.168.10.43=  |
| PM 2  | HASSANI Mohamed      | =192.168.1.23=    | =192.168.10.44=  | =192.168.10.45=  |
| PM 2  | JEAN Mathilde        | =192.168.1.24=    | =192.168.10.46=  | =192.168.10.47=  |
| PM 2  | PARANT Louison       | =192.168.1.25=    | =192.168.10.48=  | =192.168.10.49=  |
| PM 2  | SAOULA Zakariya      | =192.168.1.26=    | =192.168.10.50=  | =192.168.10.51=  |
| PM 2  | ZBOROWSKI Lucas      | =192.168.1.27=    | =192.168.10.52=  | =192.168.10.53=  |

| Groupe      | Nom                  | IP publique proxy | IP privée proxy  | IP application   |
|-------------+----------------------+-------------------+------------------+------------------|
| PM 3  | ASTOLFI Vincent      | =192.168.1.28=    | =192.168.10.54=  | =192.168.10.55=  |
| PM 3  | BEDOURET Lucie       | =192.168.1.29=    | =192.168.10.56=  | =192.168.10.57=  |
| PM 3  | CHAZOT Thomas        | =192.168.1.30=    | =192.168.10.58=  | =192.168.10.59=  |
| PM 3  | DUFOUR Louis         | =192.168.1.31=    | =192.168.10.60=  | =192.168.10.61=  |
| PM 3  | HASSOU Rayhân        | =192.168.1.32=    | =192.168.10.62=  | =192.168.10.63=  |
| PM 3  | JOLYS Enzo           | =192.168.1.33=    | =192.168.10.64=  | =192.168.10.65=  |
| PM 3  | LACHENAL Johan       | =192.168.1.34=    | =192.168.10.66=  | =192.168.10.67=  |
| PM 3  | LANONE Maxence       | =192.168.1.35=    | =192.168.10.68=  | =192.168.10.69=  |
| PM 3  | LEVADOUX Tim         | =192.168.1.36=    | =192.168.10.70=  | =192.168.10.71=  |
| PM 3  | MIELCAREK Félix      | =192.168.1.37=    | =192.168.10.72=  | =192.168.10.73=  |
| PM 3  | MOURGAND Chloé       | =192.168.1.38=    | =192.168.10.74=  | =192.168.10.75=  |
| PM 3  | RANDON Noan          | =192.168.1.39=    | =192.168.10.76=  | =192.168.10.77=  |
| PM 3  | REGNAULT Rémi        | =192.168.1.40=    | =192.168.10.78=  | =192.168.10.79=  |
| PM 3  | VERDIER Nathan       | =192.168.1.41=    | =192.168.10.80=  | =192.168.10.81=  |

| Groupe      | Nom                  | IP publique proxy | IP privée proxy  | IP application   |
|-------------+----------------------+-------------------+------------------+------------------|
| WEB 1 | ARGOUT Owen          | =192.168.1.42=    | =192.168.10.82=  | =192.168.10.83=  |
| WEB 1 | BARLET Tristan       | =192.168.1.43=    | =192.168.10.84=  | =192.168.10.85=  |
| WEB 1 | BENJELLOUN Othmane   | =192.168.1.44=    | =192.168.10.86=  | =192.168.10.87=  |
| WEB 1 | BRETTE Laurine       | =192.168.1.45=    | =192.168.10.88=  | =192.168.10.89=  |
| WEB 1 | CARREAU Alexis       | =192.168.1.46=    | =192.168.10.90=  | =192.168.10.91=  |
| WEB 1 | CARVALHEIRO Justin   | =192.168.1.47=    | =192.168.10.92=  | =192.168.10.93=  |
| WEB 1 | DUPIN Théo           | =192.168.1.48=    | =192.168.10.94=  | =192.168.10.95=  |
| WEB 1 | FILLOT Romain        | =192.168.1.49=    | =192.168.10.96=  | =192.168.10.97=  |
| WEB 1 | FRIZOT Colin         | =192.168.1.50=    | =192.168.10.98=  | =192.168.10.99=  |
| WEB 1 | LAMANDE Alexis       | =192.168.1.51=    | =192.168.10.100= | =192.168.10.101= |
| WEB 1 | MEURET Justine       | =192.168.1.52=    | =192.168.10.102= | =192.168.10.103= |
| WEB 1 | PIERRON Joan         | =192.168.1.53=    | =192.168.10.104= | =192.168.10.105= |
| WEB 1 | ROCHELLE Hugo        | =192.168.1.54=    | =192.168.10.106= | =192.168.10.107= |
| WEB 1 | SABATIER Audric      | =192.168.1.55=    | =192.168.10.108= | =192.168.10.109= |
| WEB 1 | VALADE Lou           | =192.168.1.56=    | =192.168.10.110= | =192.168.10.111= |

| Groupe | Nom              | IP publique proxy | IP privée proxy  | IP application   |
|--------+------------------+-------------------+------------------+------------------|
| WEB 2  | ARNAL Rémi       | =192.168.1.57=    | =192.168.10.112= | =192.168.10.113= |
| WEB 2  | BAVEREL Baptiste | =192.168.1.58=    | =192.168.10.114= | =192.168.10.115= |
| WEB 2  | BESSON Jérémy    | =192.168.1.59=    | =192.168.10.116= | =192.168.10.117= |
| WEB 2  | FRÉVILLE Clément | =192.168.1.60=    | =192.168.10.118= | =192.168.10.119= |
| WEB 2  | GARNIER Noé      | =192.168.1.61=    | =192.168.10.120= | =192.168.10.121= |
| WEB 2  | HODIN Dorian     | =192.168.1.62=    | =192.168.10.122= | =192.168.10.123= |
| WEB 2  | JAULT Aurian     | =192.168.1.63=    | =192.168.10.124= | =192.168.10.125= |
| WEB 2  | LAPORTE Clément  | =192.168.1.64=    | =192.168.10.126= | =192.168.10.127= |
| WEB 2  | MARCEL Baptiste  | =192.168.1.65=    | =192.168.10.128= | =192.168.10.129= |
| WEB 2  | MAZINGUE Matis   | =192.168.1.66=    | =192.168.10.130= | =192.168.10.131= |
| WEB 2  | OLLIER Bastien   | =192.168.1.67=    | =192.168.10.132= | =192.168.10.133= |
| WEB 2  | PRADIER Hugo     | =192.168.1.68=    | =192.168.10.134= | =192.168.10.135= |
| WEB 2  | RICHARD Corentin | =192.168.1.69=    | =192.168.10.136= | =192.168.10.137= |
| WEB 2  | THIERY Marc      | =192.168.1.70=    | =192.168.10.138= | =192.168.10.139= |
