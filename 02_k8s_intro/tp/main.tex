% Created 2024-05-24 ven. 09:37
% Intended LaTeX compiler: pdflatex
\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage[a4paper,margin=0.5in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[inkscapelatex=false]{svg}
\usepackage[sfdefault]{AlegreyaSans}
\usepackage{multicol}
\usepackage{minted}
\usepackage{float}
\usepackage{tikz}
\usetikzlibrary{positioning}
\renewcommand\listingscaption{Exemple de code}
\usepackage[tikz]{bclogo}
\usepackage{tcolorbox}
\tcbuselibrary{skins}
\usepackage{ragged2e}
\usepackage{environ}
\date{}
\title{TP1 - Cours de virtualisation avancée - \emph{Kubernetes}, introduction}
\hypersetup{
 pdfauthor={Evrard Van Espen},
 pdftitle={TP1 - Cours de virtualisation avancée - \emph{Kubernetes}, introduction},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 30.0.50 (Org mode 9.6.15)}, 
 pdflang={English}}
\begin{document}

\maketitle
\NewEnviron{warning}%
  {\begin{center}%
  \begin{tcolorbox}[notitle,
    colback=orange!5!white,
    colbacklower=white,
    frame hidden,
    boxrule=0pt,
    bicolor,
    sharp corners,
    borderline west={4pt}{0pt}{orange!50!black},
    fontupper=\sffamily]
     \textcolor{orange!50!black}{
        \sffamily
        \textbf{Attention:\\}%
    }%
    \BODY
  \end{tcolorbox}
  \end{center}%
  }

\NewEnviron{good}%
  {\begin{center}%
  \begin{tcolorbox}[notitle,
    colback=green!5!white,
    colbacklower=white,
    frame hidden,
    boxrule=0pt,
    bicolor,
    sharp corners,
    borderline west={4pt}{0pt}{green!50!black},
    fontupper=\sffamily]
     \textcolor{green!50!black}{
        \sffamily
    }%
    \BODY
  \end{tcolorbox}
  \end{center}%
  }  

L'objectif de ce TP est de découvrir l'orchestrateur \emph{"Kubernetes"}.

Pour cela, vous allez déployer quelques conteneurs et lancer des commandes pour découvrir le fonctionnement de \emph{Kubernetes}.

Ces conteneurs auront été configurés par mes soins et vous n'aurez donc pas de fichier à éditer.

Pour les TPs de \emph{Kubernetes}, vous allez utiliser un outil nommé \emph{"minikube"}.
Cet outil permet la mise en place de \emph{"clusters"} \emph{K8S} dédiés aux tests.
Ce ne sont pas des \emph{clusters} utilisables sereinement en production mais conçus pour permettre la découverte et les tests sans devoir s’embarrasser d'un "vrai" \emph{cluster}, compliqué à mettre en place soit même.

\section{Mise en place}
\label{sec:orgbdb8cd3}
Vous allez travailler dans une machine virtuelle \emph{VDN}.
De cette façon, vous pourrez utiliser \emph{Docker}, ce qui est nécessaire pour les TPs.

\medskip

Commencez par lancer une machine virtuelle \emph{VDN}:

\begin{minted}[]{bash}
vdn-start -t -n docker debian-1
\end{minted}

\begin{warning}
À chaque fois que vous vous connectez à la machine virtuelle, n'oubliez pas de lancer la commande suivante, sinon vous ne pourrez pas utiliser l'outil \texttt{kubectl} qui est l'outil de gestion de \emph{K8S} !

\texttt{export no\_proxy="127.0.0.1,.vdn,localhost,192.168.49.1/24,10.96.0.0/12"}
\end{warning}


\textbf{Dans un autre terminal}, connectez vous à la machine virtuelle en \texttt{root}:
\begin{minted}[]{bash}
vdn-ssh root@debian-1
\end{minted}

Puis ajouter l'utilisateur \texttt{test} au \emph{sudoers} pour lui permettre de lancer des commandes en tant que \texttt{root}.
Pour cela, ajoutez à la fin du fichier \texttt{/etc/sudoers} la ligne:
\begin{verbatim}
test ALL=(ALL:ALL) ALL
\end{verbatim}

Puis changez le mot de passe de l'utilisateur \texttt{test} \textbf{et mémorisez ce mot de passe, vous en aurez besoin}:
\begin{minted}[]{bash}
passwd test
\end{minted}


\bigskip

Ensuite, \textbf{dans un autre terminal}, connectez-vous via \emph{SSH} à cette machine virtuelle:
\begin{minted}[]{bash}
vdn-ssh test@debian-1
\end{minted}

Clonez le dépôt du TP dans la machine virtuelle:
\begin{minted}[]{bash}
git clone \
https://codefirst.iut.uca.fr/git/evrard.van_espen/cours_virtualisation_avancee_ETUD.git
\end{minted}


Ensuite, lancez le script \texttt{01\_init.sh} présent dans le dépôt pour installer \emph{minikube}.

Une fois que le script est fini et que \emph{minikube} a fini de démarrer, lancez cette commande:
\begin{minted}[]{bash}
./minikube status
\end{minted}

Le résultat de la commande doit être:
\begin{verbatim}
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
\end{verbatim}

Puis lancez:
\begin{minted}[]{bash}
./kubectl get namespaces
\end{minted}

Le résultat de la commande doit être:
\begin{verbatim}
NAME              STATUS   AGE
default           Active   11m
kube-node-lease   Active   11m
kube-public       Active   11m
kube-system       Active   11m
\end{verbatim}

Enfin, \textbf{dans un autre terminal}, lancez:
\begin{minted}[]{bash}
./minikube tunnel
\end{minted}

Cela va vous demander le mot de passe que vous avez attribué à l'utilisateur \texttt{test}.
Ce terminal doit afficher:
\begin{verbatim}
Status:	
	machine: minikube
	pid: 10418
	route: 10.96.0.0/12 -> 192.168.49.2
	minikube: Running
	services: []
    errors:
		minikube: no errors
		router: no errors
		loadbalancer emulator: no errors
\end{verbatim}

Laissez ce terminal tourner, \textbf{n'y touchez plus !}

\begin{good}
Si tout s'est déroulé comme il faut, cela veut dire que tout est installé correctement et que vous pourrez faire les TPs comme il faut.
\end{good}

\section{Votre mission}
\label{sec:org63f26c4}
Vous allez découvrir comment gérer \emph{K8S} au moyen de son outil ligne de commande \texttt{kubectl}.
Vous allez donc devoir déployer des ressources et vérifier leur fonctionnement.

\subsection{Déployer et vérifier les ressources}
\label{sec:org1d15ba0}
Les ressources sont configurées dans le fichier du dépôt \texttt{02\_ressources/01\_all.yaml}, jetez-y un œil ;)

Ce fichier défini un \emph{pod} et un \emph{service} permettant l'accès au service présent dans le \emph{pod}.
L'accès se fait via un \emph{NodePort}, qui permet l'ouverture d'un port local sur la machine à destination du service \emph{HTTP} présent dans le \emph{pod}.
Cependant, ce n'est pas une belle manière de faire les choses, vous changerez cela dans la seconde partie de ce TP.

Pour commencer, appliquez les configurations du fichier.
Ensuite, listez les \emph{pods}, vous devez obtenir un résultat similaire à:
\begin{verbatim}
NAME                                READY   STATUS    RESTARTS   AGE
hello-world-6cffc7d974-5pgc8   1/1     Running   0          9m38s
\end{verbatim}

Puis listez les services, vous devez obtenir un résultat similaire à:
\begin{verbatim}
NAME          TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
hello-world   NodePort    10.109.243.202   <none>        4242:30000/TCP   8s
\end{verbatim}

Si c'est le cas, bravo, vous avez déployé vos premiers \emph{pods} et \emph{services}.
Vous pouvez tentez de communiquer avec le service en tapant:
\begin{minted}[]{bash}
curl http://192.168.49.2:30000
\end{minted}

Si tout s'est bien passé, vous devriez avoir un résultat tel que:
\begin{verbatim}
Request served by hello-world-bdd88c654-kcg8p

HTTP/1.1 GET /

Host: 10.96.123.46
Accept: */*
User-Agent: curl/7.88.1
\end{verbatim}

\subsection{Maintenant, à vous de jouer}
\label{sec:org419d3df}
À présent, vous allez devoir modifier la configuration pour rendre tout cela un peu plus "propre".

Tâches à réaliser:
\begin{itemize}
\item ne plus utiliser un simple \emph{Pod} mais plutôt un \emph{Deployment} avec un nombre de \emph{replicas} à 3;
\item ne pas utiliser un \emph{NodePort} mais plutôt un \emph{LoadBalancer} qui redirigea le trafic sur les différents \emph{Pods} du \emph{Deployment}.
\end{itemize}

Pour vérifier le bon fonctionnement du \emph{LoadBalancer}, commencez par récupérer son adresse "externe" et vous pourrez faire:
\begin{minted}[]{bash}
curl http://<external ip>
\end{minted}

Cela vous donnera une sortie telle que:
\begin{verbatim}
Request served by hello-world-<PARTIE QUI CHANGE>

HTTP/1.1 GET /

Host: 10.101.132.184
Accept: */*
User-Agent: curl/7.88.1
\end{verbatim}

Si \texttt{<PARTIE QUI CHANGE>} est différente d'une requête à l'autre, c'est que votre mission est réussie !

\section{À l’aide !}
\label{sec:org834e602}
Basez-vous sur le cours ainsi que les documentations suivantes pour y parvenir:
\begin{itemize}
\item \url{https://kubernetes.io/fr/docs/concepts/services-networking/service/}
\item \url{https://kubernetes.io/docs/concepts/workloads/pods/}
\item \url{https://kubernetes.io/docs/concepts/workloads/controllers/deployment/}
\end{itemize}
\end{document}
